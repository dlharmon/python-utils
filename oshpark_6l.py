#!/usr/bin/env python3
import atlc

mm_to_mil = 1000/25.4

def l2ref13(w,s, scale=10):
    print(f"calculating l2 ref l1, l3 w = {w}, s={s}")
    # unit = 0.1 mil
    atlc.calc_diff_line(
        box_width=500,
        conductor_width = int(round(scale*w)),
        conductor_gap = int(round(scale*s)),
        conductor_thickness = int(round(scale*0.6)),
        dielectric_lower_thickness = int(round(4*scale)),
        dielectric_lower_Er=3.66,
        dielectric_upper_thickness = int(round((4.36+0.6)*scale)),
        dielectric_upper_Er=3.23,
    )

def l3ref24(w,s, scale=10):
    print(f"calculating l3 ref l2, l4 w = {w}, s={s}")
    # unit = 0.1 mil
    atlc.calc_diff_line(
        box_width=scale*50,
        conductor_width = int(round(scale*w)),
        conductor_gap = int(round(scale*s)),
        conductor_thickness = int(round(scale*0.6)),
        dielectric_lower_thickness = int(round(4*scale)),
        dielectric_lower_Er=3.66,
        dielectric_upper_thickness = int(round((0.6+7.72+21)*scale)),
        dielectric_upper_Er=3.7,
    )


def l1refl2(w,s, scale=10):
    atlc.calc_diff_line(
        box_width=500,
        conductor_width = 67,
        conductor_gap = 50,
        conductor_thickness = 18,
        dielectric_lower_thickness = 44,
        dielectric_lower_Er=3.2,
        dielectric_upper_thickness = 200,
        dielectric_upper_Er=1.0,
        mask_thickness=6,
        mask_Er=3.9
    )

# 90
#l2ref13(5,8,scale=20)
# 90
#l3ref24(5,5,scale=20)
l3ref24(6,8,scale=20)
# 100
#l3ref24(5,8.8,scale=20)

# 50
#l3ref24(0.15*mm_to_mil,0,scale=20)
