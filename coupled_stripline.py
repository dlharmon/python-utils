#!/usr/bin/env python3
import atlc

#Zoe [210.56484748 151.40716686 134.38545356 131.72295487 130.99391173]
#Zoo [ 77.70021085  76.58478058  80.26153775  81.09366838  81.33575227]
# w  [ 16           33           39           40           40]
# s  [ 13           21           29           31           32]
# Ere[  1.235        1.185       1.178        1.175        1.175
# Ero[  1.592        1.430       1.375        1.365        1.362

#Zoe [211.70979645 152.44488748 135.19291267 132.77455377]
#Zoo [ 77.8638673   76.43933361  80.0247295   80.75540528]
# w/s   16/13 32/21 39/29 40/31

# coupler
# w s zo ze Ero Ere
# 140 12 39.398 63.685 1.28 1.1
# 142 13 39.724 62.824
# 143 14

# unit = 0.00025"
atlc.calc_interleaved_stripline(
    box_width= 316*2,
    conductor_width = 78,
    space = 65,
    n = 1,
    conductor_offset = 0,
    conductor_thickness = 8,
    dielectric_lower_thickness = 63*2,
    dielectric_outer_Er=1,
    dielectric_middle_thickness=8*2,
    dielectric_middle_Er=3.2,
    dielectric_upper_thickness = 63*2,
    diff=True,
    #both_sides=True,
    #side_ground=12
    #center_hole=136
)
