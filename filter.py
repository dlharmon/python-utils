from math import tan, cosh, sinh, tanh, log, pi, sqrt, atanh
from scipy.special import ellipk
import numpy as np
import scipy.optimize
from chebyshev import chebyshev, coth

# return ellipk(sqrt(1-x**2)/ellipk(x) (approx +- 8 ppm)
def ellipfun(x, exact=True):
    if x**2 > 1:
        assert()
    xp = sqrt(1-x*x)
    if exact:
        return ellipk(xp)/ellipk(x)
    if x**2 < 0.5:
        return log(2*(1+sqrt(xp))/(1-sqrt(xp)))/pi
    return pi/log(2*(1+sqrt(x))/(1-sqrt(x)))

def k_to_z(Er, k):
    return 30*pi*ellipfun(k)/sqrt(Er)

# Matthaei, Young, Jones 5.05
def thin_sl_z(w, s, b, Er):
    k = tanh(pi*w/(2*b))
    ke = k*tanh(0.5*pi*(w+s)/b)
    ko = k*coth(0.5*pi*(w+s)/b)
    Zoe = k_to_z(Er, ke)
    Zoo = k_to_z(Er, ko)
    return Zoe, Zoo

def thin_sl_w_s(Zoe, Zoo, Er, b):
    ke = z_to_k(Er, Zoe)
    ko = z_to_k(Er, Zoo)
    print("ko, ke", ko, ke)
    w = 2*b*atanh(sqrt(ko*ke))/pi
    s = 2*b*atanh(((1-ko)/(1-ke))*sqrt(ke/ko))/pi
    return w, s

def thin_sl_w_s(Zoe, Zoo, Er, b):
    def F(k):
        w = k[0]
        s = k[1]
        Zoea, Zooa = thin_sl_z(w,s,b,Er)
        return (Zoea-Zoe)**2 + (Zooa-Zoo)**2
    x = scipy.optimize.minimize(F, [b, 0.25*b], bounds = [[1e-3*b, 3*b], [1e-3*b, 3*b]])
    return x.x

# Matthaei, Young, Jones 8.09
def ec_sl_simple(fc, bw, n, Lar, Er=2.55, b=0.5, Zo=50):
    g = chebyshev(n, Lar)
    w = bw/fc
    # J[0] = J01/Y0
    J = np.zeros(n+1)
    J[0] = sqrt(pi*w/(2*g[0]*g[1]))
    J[-1] = sqrt(pi*w/(2*g[-2]*g[-1]))
    for i in range(1,n):
        J[i] = pi*w/(2*sqrt(g[i]*g[i+1]))
    Zoe = Zo * (1 + J + J**2)
    Zoo = Zo * (1 - J + J**2)
    print("Zoo", Zoo)
    print("Zoe", Zoe)
    for i in range(len(Zoe)):
        print("w, s", i, thin_sl_w_s(Zoe[i], Zoo[i], Er, b))
    return J

# Matthaei, Young, Jones Table 10.02-1
def ec_sl(fc, bw, n, Lar, Er=2.55, b=0.5, Zo=50, h=None):
    g =  chebyshev(n, Lar)
    w = bw/fc
    w1p = 1.0
    theta1 = 0.5*pi*(1-w/2)
    Ya = 1/Zo
    # J[0] = J01/Ya
    J = np.zeros(n+1)
    Yoo = np.zeros(n+1)
    Yoe = np.zeros(n+1)
    J[0] = 1/sqrt(g[0]*g[1]*w1p)
    J[-1] = 1/sqrt(g[-2]*g[-1]*w1p)
    if h is None:
        h = 1/(tan(theta1)/2 + (J[0]**2))
    print("h =", h, "w =", w)
    Yoo[0] = Ya*(J[0]*sqrt(h) + 1)
    Yoo[-1] = Ya*(J[-1]*sqrt(h) + 1)
    Yoe[0] = 2*Ya - Yoo[0]
    Yoe[-1] = 2*Ya - Yoo[-1]
    for i in range(1,n):
        J[i] = 1/(w1p*sqrt(g[i]*g[i+1]))
        N = sqrt(J[i]**2 + tan(theta1)**2/4)
        Yoo[i] = h*Ya*(N+J[i])
        Yoe[i] = h*Ya*(N-J[i])
    print("Yoo", Yoo)
    print("Yoe", Yoe)
    Zoe = Yoo/(Ya**2)
    Zoo = Yoe/(Ya**2)
    YoeB = Yoe[0] + h*Ya*(tan(theta1)/2+J[0]**2-Ya)
    YooB = YoeB + Yoo[0] - Yoe[0]
    ZoeB = YooB/(Ya**2)
    ZooB = YoeB/(Ya**2)
    print("YoeB, YooB", YoeB, YooB)
    print("ZoeB, ZooB", ZoeB, ZooB)
    print("Zoo", Zoo)
    print("Zoe", Zoe)
    for i in range(len(Zoe)):
        print("w, s", i, thin_sl_w_s(Zoe[i], Zoo[i], Er, b))
    return J

# Matthaei, Young, Jones 10.06-1
def idbpf(fc, bw, n, Lar, Er=1.0, h=0.015, Zo=50, Qu=None):
    g = chebyshev(n, Lar)
    w = bw/fc
    if Qu is not None:
        loss_db = 4.343 * (1.0/w) * np.sum(g) / Qu
        print("loss in dB, estimated = ", loss_db)
    theta1 = np.pi*0.5*(1-w/2.0)
    w1p = 1.0
    Ya = 1/Zo
    # J[0] = J01/Ya
    J = np.zeros(n+1)
    N = np.zeros(n-1) # N[0] = N1,2
    Cself = np.zeros(n+2) # [0] = C0/E
    Cmut = np.zeros(n+1) # [0] = C01/E
    J[0] = 1.0/(w1p*sqrt(g[0]*g[1]))
    J[-1] = 1.0/(w1p*sqrt(g[-2]*g[-1]))
    for i in range(1,n):
        J[i] = 1.0/sqrt(g[i]*g[i+1]*w1p)
    for i in range(n-1):
        N[i] = sqrt(J[i+1]**2+tan(theta1)**2/4)
    M1 = Ya*(J[0]*sqrt(h)+1)
    Mn = Ya*(J[-1]*sqrt(h)+1)
    Cself[0] = (376.7/sqrt(Er)) * (2*Ya - M1)
    Cself[1] = (376.7/sqrt(Er)) * (Ya + -M1 + h*Ya*(tan(theta1)/2 + J[0]**2 + N[0] - J[1]))
    for i in range(2,n):
        Cself[i] = (376.7/sqrt(Er)) * h*Ya*(N[i-2]+N[i-1]-J[i-1]-J[i])
    Cself[-2] = (376.7/sqrt(Er)) * (Ya + -Mn + h*Ya*(tan(theta1)/2 + J[-1]**2 + N[-1] - J[-2]))
    Cself[-1] = (376.7/sqrt(Er)) * (2*Ya - Mn)
    Cmut[0]  = (376.7/sqrt(Er)) * (M1-Ya)
    Cmut[-1] = (376.7/sqrt(Er)) * (Mn-Ya)
    for i in range(1,n):
        Cmut[i] = (376.7/sqrt(Er)) * h * Ya * J[i]
    print("theta1 =", theta1)
    print("J =", J)
    print("N =", N)
    print("M =", M1, Mn)
    print("Cself =", Cself)
    print("Cmut =", Cmut)
    Zoe = 376.7 / (sqrt(Er) * Cself)
    print("Zoe", Zoe)
    Yoo = 1/Zoe[:-1] + sqrt(Er) * 2 * Cmut/376.7
    Zoo = 1/Yoo
    print("Zoo", Zoo)
    Yoo = 1/Zoe[1:] + sqrt(Er) * 2 * Cmut/376.7
    Zoo = 1/Yoo
    print("Zoo", Zoo)

# Matthaei, Young, Jones 10.07-1
def idbpf_wide(fc, bw, n, Lar, Er=1.0, h=0.015, Zo=50, Qu=None):
    g = chebyshev(n, Lar)
    w = bw/fc
    if Qu is not None:
        loss_db = 4.343 * (1.0/w) * np.sum(g) / Qu
        print("loss in dB, estimated = ", loss_db)
    theta1 = np.pi*0.5*(1-w/2.0)
    w1p = 1.0
    Ya = 1/Zo
    # J[0] = J01/Ya
    J = np.zeros(n)
    for i in range(2,n-1):
        J[i] = g[2]/(g[0]*sqrt(g[i]*g[i+1]))
    # note: J[n-2,n-1] has it's own equation in the book, not required with symmetry
    N = np.zeros(n) # N[0] = N0,1
    for i in range(2,n-1):
        N[i] = sqrt(J[i]**2+(g[2]*tan(theta1)/(2*g[0]))**2)

    # making some assumptions about symmetry, so check that it really is
    assert abs(g[0]*g[1] - g[-1]*g[-2]) < 1e-6

    Z1_ZA = g[0]*g[1]*tan(theta1)
    Yk_YA = np.zeros(n)
    Yk_YA[2] = g[2]/(2*g[0])*tan(theta1) + N[2] - J[2]
    for i in range(3,n-1):
        Yk_YA[i] = N[i-1]+N[i] - J[i-1]-J[i]

    Cself = np.zeros(n) # [0] = C1/E
    Cself[0] = Cself[-1] = 376.7/sqrt(Er) * Ya * (1 - sqrt(h))/(Z1_ZA)
    Cself[1] = Cself[-2] = 376.7/sqrt(Er) * Ya * h * (Yk_YA[2]) - sqrt(h)*Cself[0]
    for i in range(3,n-1):
        Cself[i-1] = 376.7/sqrt(Er) * Ya * h * (Yk_YA[i])

    Cmut = np.zeros(n-1) # [0] = C01/E
    Cmut[0] = Cmut[-1] = 376.7/sqrt(Er)*Ya*sqrt(h)/Z1_ZA#C12/E
    for i in range(2,n-1):
        Cmut[i-1] = 376.7/sqrt(Er)*Ya*h*J[i]

    print("theta1 =", theta1)
    print("J =", J)
    print("N =", N)
    print("Cself =", Cself)
    print("Cmut =", Cmut)
    Zoe = 376.7 / (sqrt(Er) * Cself)
    print("Zoe", Zoe)
    Yoo = 1/Zoe[:-1] + sqrt(Er) * 2 * Cmut/376.7
    Zoo = 1/Yoo
    print("Zoo", Zoo)
    Yoo = 1/Zoe[1:] + sqrt(Er) * 2 * Cmut/376.7
    Zoo = 1/Yoo
    print("Zoo", Zoo)

#print(ec_sl(1207e6, 120.7e6, 6, 0.01))
print("book example 10.02-2")
#ec_sl(1e9, 300e6, 6, 0.1, 1.0, 2.0, Zo=50)
#ec_sl_simple(16.95e9, 5e9, 7, 0.05, Er=1.3, b=63+8+63, Zo=100)
#ec_sl_simple(16.95e9, 5e9, 7, 0.05, Er=1.3, b=1.7, Zo=50)
#idbpf(4e9, 200e6, 5, 0.05, Er=1, h=0.1)
print("book example 10.07-2")
idbpf_wide(1.5e9, 1.5e9*.7, 8, 0.1, Er=1, h=0.18)
