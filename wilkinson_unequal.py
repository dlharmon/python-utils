#!/usr/bin/env python3
import numpy as np
import math
import sys, argparse

parser = argparse.ArgumentParser()
parser.add_argument('--freq', type=float, help="center frequency", default=1.0e9)
parser.add_argument('--ratio', type=float, help="ratio of power outputs", default=1.0)
parser.add_argument('--zo', type=int, help="port impedance", default=50)
args = parser.parse_args()

f = args.freq
w = 2*np.pi*f
pr = args.ratio
zo = args.zo

# https://www.microwaves101.com/calculators/871-unequal-split-power-divider-calculator
# a is first line in HP path, c is 2nd
# b is first line in LP path, d is 2nd

zoa = zo * (pr**-1.5 + pr**-0.5)**0.5
zob = zo * (1+pr)**0.5 * pr**0.25
zoc = zo * (pr)**-0.25
zod = zo * pr**0.25
rw = zo * (pr**0.5 + pr**-0.5)

print("Zoa = ", zoa)
print("Zob = ", zob)
print("Zoc = ", zoc)
print("Zod = ", zod)
print("Rw = ", rw)

# capacitance on each end of line
ca = 1 / (zoa*w)
# line inductance
la = zoa / w

cb = 1 / (zob*w)
lb = zob / w
cc = 1 / (zoc*w)
lc = zoc / w
cd = 1 / (zod*w)
ld = zod / w

print("cin = ", ca+cb)
print("ch1 = ", ca+cc)
print("ch2 = ", cc)
print("cl1 = ", cb+cd)
print("cl2 = ", cd)
print("lh1 = ", la)
print("lh2 = ", lc)
print("ll1 = ", lb)
print("ll2 = ", ld)
