#!/usr/bin/env python3
import atlc

# unit = 0.5 mil
def calc(w):
    return atlc.calc_broadside_stripline(
        box_width=316,
        conductor_width = w,
        conductor_offset = 0,
        conductor_thickness = 4,
        dielectric_lower_thickness = 63,
        dielectric_outer_Er=1.0,
        dielectric_middle_thickness=8,
        dielectric_middle_Er=3.2,
        dielectric_upper_thickness = 63,
        diff=False,
        #both_sides=True,
        both_top=False,
    )

w = range(20,200)
import numpy as np
er = np.zeros(len(w))
z = np.zeros(len(w))

import json
data = {}

for i in range(len(w)):
    [er[i], z[i]] = calc(w[i])
    print(w[i], er[i], z[i])

data['w']  = (25.4e-6*np.array(w)/2.0).tolist()
data['Er'] = er.tolist()
data['Z']  = z.tolist()
with open('sss_w_z.json', 'w') as f:
    json.dump(data, f)
