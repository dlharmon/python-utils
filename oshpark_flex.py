#!/usr/bin/env python3
import atlc

# unit = 1 um
atlc.calc_diff_line(
    box_width=1200,
    conductor_width = 195,
    conductor_gap = 0,
    conductor_thickness = 35,
    dielectric_lower_thickness = 102,
    dielectric_lower_Er=3.2,
    dielectric_upper_thickness = 1000,
    dielectric_upper_Er=1.0,
    mask_thickness=25,
    mask_Er=3.3
)
