#!/usr/bin/env python3

from PIL import Image, ImageDraw
import sys, os, subprocess

def calc_line(
        box_width=6100,
        conductor_width=61,
        conductor_thickness=6,
        dielectric1_thickness=40,
        dielectric1_Er=3.66,
        dielectric2_thickness=77,
        dielectric2_Er=3.61,
        dielectric3_thickness=210,
        dielectric3_Er=3.79,
        dielectric4_thickness=77, # Er= same as 2
):
    ygp = 1 # one pixel ground boundary
    y1 = ygp + dielectric1_thickness
    y2 = y1 + conductor_thickness
    y3 = y1 + dielectric2_thickness
    y4 = y3 + dielectric3_thickness
    y5 = y4 + dielectric4_thickness
    ymax = y5+1
    im = Image.new('RGB', (box_width,ymax), 0x00FF00) # all ground at start
    draw = ImageDraw.Draw(im)
    # fill with dielectric
    xm = box_width-2
    draw.rectangle([1,1,xm,y1-1], 0xE0E0E0)
    draw.rectangle([1,y1,xm,y3-1], 0x00CAFE)
    draw.rectangle([1,y3,xm,y4-1], 0x00BEEF)
    draw.rectangle([1,y4,xm,y5-1], 0x00CAFE)
    # conductors
    hw = box_width//2
    x1 = hw - conductor_width//2
    x2 = x1 + (conductor_width)
    draw.rectangle([x1, y1, x2-1, y2-1], 0x0000FF)
    im.save('test.bmp')
    rv = subprocess.check_output("atlc -s -S -d e0e0e0={} -d feca00={} -d efbe00={} test.bmp".format(
        dielectric1_Er, dielectric2_Er, dielectric3_Er), shell=True)
    print(rv.split())
    return rv

if __name__ == "__main__":
    calc_line()
