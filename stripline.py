#!/usr/bin/env python3
import atlc

# in wide channel
w = 47 # for 100 ohm single sided
w = 165 # for 50 ohm double sided

# 0.75 mm air on either side of OSHPark flex in 4 mm wide channel
w = 47 # for 100 ohm single sided
w = 161 # for 50 ohm double sided

# unit = 1 um
print(atlc.calc_broadside_stripline(
    box_width=316,
    conductor_width = w,
    conductor_offset = 0,
    conductor_thickness = 4,
    dielectric_lower_thickness = 63,
    dielectric_outer_Er=1.0,
    dielectric_middle_thickness=8,
    dielectric_middle_Er=3.2,
    dielectric_upper_thickness = 63,
    diff=False,
    #both_sides=True,
    both_top=False,
))
