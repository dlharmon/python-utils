#!/usr/bin/env python3

from PIL import Image, ImageDraw
import sys, os, subprocess

def calc_diff_line(
        box_width,
        conductor_width,
        conductor_gap = 0,
        conductor_thickness=3,
        dielectric_lower_thickness=51,
        dielectric_lower_Er=3.2,
        dielectric_upper_thickness=66,
        dielectric_upper_Er=3.2,
        mask_thickness=0,
        mask_Er=3.3,
):
    ygp = 1 # one pixel ground boundary
    ydiel1 = ygp + dielectric_lower_thickness
    ymst = ydiel1 + conductor_thickness
    ydiel2t = ydiel1 + dielectric_upper_thickness
    ymax = ydiel2t+1

    im = Image.new('RGB', (box_width,ymax), 0x00FF00) # all ground at start
    draw = ImageDraw.Draw(im)
    # fill with dielectric
    draw.rectangle([1,1,box_width-2,ydiel2t-2], 0xE0E0E0)
    draw.rectangle([1,ydiel1,box_width-2,ydiel2t-1], 0x00CAFE)
    if mask_thickness != 0:
        draw.rectangle([1,ydiel1,box_width-2,ydiel1+mask_thickness+1], 0x00BEEF)
    # conductors
    hw = int(box_width/2)
    if conductor_gap != 0:
        x0 = hw - (conductor_gap//2 + conductor_width)
        x1 = x0 + conductor_width
        x2 = x1 + conductor_gap
        x3 = x2 + conductor_width
        draw.rectangle([x0, ydiel1, x1 - 1, ymst - 1], 0x0000FF)
        draw.rectangle([x2, ydiel1, x3 - 1, ymst - 1], 0xFF0000)
    else:
        draw.rectangle([hw - conductor_width/2, ydiel1,
                        hw + (1 + conductor_width/2), ymst - 1], 0x0000FF)
    im.save('test.bmp')
    # fixme: return impedance data
    rv = subprocess.check_output("atlc -s -S -d e0e0e0={} -d feca00={} -d efbe00={} test.bmp".format(
        dielectric_lower_Er, dielectric_upper_Er, mask_Er), shell=True)
    print(rv.split())
    return rv

def calc_broadside_stripline(
        box_width,
        conductor_width,
        conductor_offset = 0,
        conductor_thickness=3,
        dielectric_lower_thickness=80,
        dielectric_outer_Er=3.2,
        dielectric_middle_thickness=8,
        dielectric_middle_Er=3.2,
        dielectric_upper_thickness=80,
        diff=True,
        both_top=False,
        both_sides=False,
        conductor_width_bottom = None,
):
    if conductor_width_bottom is None:
        conductor_width_bottom = conductor_width
    if (box_width % 2) != 0:
        print("warning, box_width should be a multiple of 2")
    #if (conductor_gap % 2) != 0:
    #    print("warning, conductor_gap should be a multiple of 2")
    ygp = 1 # one pixel ground boundary
    ydiel1 = ygp + dielectric_lower_thickness
    ymst1 = ydiel1 - conductor_thickness
    ydiel2 = ydiel1 + dielectric_middle_thickness
    ymst2 = ydiel2 + conductor_thickness
    ydiel3 = ydiel2 + dielectric_upper_thickness
    ymax = ydiel3+1

    im = Image.new('RGB', (box_width,ymax), 0x00FF00) # all ground at start
    draw = ImageDraw.Draw(im)
    hw = int(box_width/2)
    # fill with dielectric
    draw.rectangle([1,1,box_width-2,ydiel3-1], 0xE0E0E0)
    draw.rectangle([1,ydiel1,box_width-2,ydiel2-1], 0x00CAFE)
    # conductors
    draw.rectangle([hw + conductor_width/2 + conductor_offset, ymst1, hw + conductor_offset - conductor_width/2 - 1, ydiel1-1], 0x0000FF)
    if diff or both_sides:
        y1 = ymst1 if both_top else ydiel2
        y2 = ydiel1-1 if both_top else ymst2-1
        draw.rectangle([hw + conductor_width_bottom/2 - conductor_offset, y1, hw - conductor_offset - conductor_width_bottom/2 - 1, y2], 0x0000FF if both_sides else 0xFF0000)

    im.save('test.bmp')
    # fixme: return impedance data
    rv = subprocess.check_output("atlc -s -S -d e0e0e0={} -d feca00={} test.bmp".format(
        dielectric_outer_Er, dielectric_middle_Er), shell=True)
    print(rv.split())
    return [float(rv.split()[3]), float(rv.split()[5])]

def calc_interleaved_stripline(
        box_width,
        conductor_width,
        space,
        n, # number of fingers
        conductor_offset = 0,
        conductor_thickness=3,
        dielectric_lower_thickness=80,
        dielectric_outer_Er=3.2,
        dielectric_middle_thickness=8,
        dielectric_middle_Er=3.2,
        dielectric_upper_thickness=80,
        diff=True,
        side_ground = None,
        both_sides = False,
        center_hole = None,
):
    if (box_width % 2) != 0:
        print("warning, box_width should be a multiple of 2")
    ygp = 1 # one pixel ground boundary
    ydiel1 = ygp + dielectric_lower_thickness
    ymst1 = ydiel1 - conductor_thickness
    ydiel2 = ydiel1 + dielectric_middle_thickness
    ydiel3 = ydiel2 + dielectric_upper_thickness
    ymax = ydiel3+1
    hw = box_width//2

    im = Image.new('RGB', (box_width,ymax), 0x00FF00) # all ground at start
    draw = ImageDraw.Draw(im)
    # fill with dielectric
    draw.rectangle([1,1,box_width-2,ydiel3-1], 0xE0E0E0)
    draw.rectangle([1,ydiel1,box_width-2,ydiel2-1], 0x00CAFE)
    if center_hole is not None:
        draw.rectangle([hw-center_hole//2,ydiel1,hw+center_hole//2,ydiel2-1], 0xFFFFFF)
    # conductors
    pixels = 2*n*conductor_width + (2*n-1)*space
    x = hw - pixels//2
    y1 = ymst1
    y2 = ydiel1-1

    if side_ground is not None:
        draw.rectangle([0, y1, x-side_ground - 1, y2], 0x00FF00)

    ymst2 = ydiel2 + conductor_thickness
    y3 = ydiel2
    y4 = ymst2-1

    for i in range(n):
        draw.rectangle([x, y1, x+conductor_width - 1, y2], 0x0000FF)
        if both_sides:
            draw.rectangle([x, y3, x+conductor_width - 1, y4], 0x0000FF)
        x += conductor_width + space
        draw.rectangle([x, y1, x+conductor_width - 1, y2], 0xFF0000)
        if both_sides:
            draw.rectangle([x, y3, x+conductor_width - 1, y4], 0xFF0000)
        x += conductor_width + space

    if side_ground is not None:
        draw.rectangle([x + side_ground - space, y1, box_width - 1, y2], 0x00FF00)

    im.save('test.bmp')
    # fixme: return impedance data
    rv = subprocess.check_output("atlc -s -S -d e0e0e0={} -d feca00={} test.bmp".format(
        dielectric_outer_Er, dielectric_middle_Er), shell=True)
    print(rv.split())
    return rv

if __name__ == "__main__":
    # example usage with OSHPark 4 L stack
    # pixel unit = 2 um
    calc_diff_line(
        box_width=800,
        conductor_width = 70, # 140 um
        conductor_gap = 70, # 140 um
        conductor_thickness = 8, # 16 um
        dielectric_lower_thickness = 500, # 1 mm
        dielectric_lower_Er=4.0,
        dielectric_upper_thickness = 85, # 170 um
        dielectric_upper_Er=3.3
    )
