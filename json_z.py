#!/usr/bin/env python3
from matplotlib import pyplot as plt

import json
import numpy as np
with open('sss_w_z.json') as f:
    data = json.load(f)

w = np.array(data['w'])
Z = np.array(data['Z'])

from scipy.optimize import curve_fit

def func(x, a, b, c, d, e, f, g):
    return a + b*x + c*x**2 + d*x**3 + e*x**4 + f*x**5 + g*x**6

popt, pcov = curve_fit(func, Z, w)
print("fit = ", popt)
print("stddev = ", np.sqrt(np.diag(pcov)))

Zi = np.linspace(40,130,200)
fit = popt
wi = func(Zi, fit[0], fit[1], fit[2], fit[3], fit[4], fit[5], fit[6])

plt.plot(w,Z)
plt.plot(wi,Zi)
plt.show()

import scipy.integrate as integrate
from scipy.special import i1

def f(y,A):
    x = A*np.sqrt(1.0-y*y)
    return i1(x)/x

def fi(x,A):
    r = np.zeros(len(x))
    for i in range(len(x)):
        (r[i], err) = integrate.quad(lambda y: f(y,A), 0, x[i])
    return r

def klopfenstein(N, Za, Zb, A=2.0):
    G0 = 0.5*np.log(Zb/Za) # alternate: 0.5*(Zb-Za)/(Zb+Za)
    m = np.linspace(0,1.0,N)
    return np.exp(np.log(Za*Zb)*0.5 + G0*A*A*fi(2*m-1,A)/np.cosh(A))

Zl = klopfenstein(30, Za=50.0, Zb=100.0, A=4.0)
plt.plot(Zl)
plt.show()

Wl=func(Zl, fit[0], fit[1], fit[2], fit[3], fit[4], fit[5], fit[6])
plt.plot(Wl)
plt.show()

from openems import kicadfpwriter

for l in [5, 10, 15, 20]:
    g = kicadfpwriter.Generator("klopfenstein{}".format(l))
    n = len(Zl)
    x = np.linspace(0,l*1e-3,n)
    y = Wl*.5
    points=np.zeros((n*2,2))
    print(points)
    for i in range(n):
        points[i] = [x[i],y[i]]
        points[2*n-(i+1)] = [x[i],-y[i]]
    g.add_polygon(points*1000.0)
    g.add_pad("1", 0, 0, diameter=0.5, shape='circle')
    g.add_pad("2", l, 0, diameter=0.5, shape='circle')
    fp = g.finish()
    with open("klopfenstein_{}.kicad_mod".format(l), "w") as f:
        f.write(fp)
