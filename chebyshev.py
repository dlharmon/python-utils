import numpy as np

def coth(x):
    return np.cosh(x)/np.sinh(x)

# return a filter prototype of order n, ripple Lar (dB)
# Matthaei, Young, Jones 4.05-2,3
def chebyshev(n, Lar):
    beta = np.log(coth(Lar/17.37))
    gamma = np.sinh(beta/(2*n))
    k = np.arange(1,n+1)
    a = np.sin((2*k - 1)*np.pi/(2*n))
    b = gamma**2 + np.sin(k*np.pi/n)**2
    g = np.ones(n+2)
    g[1] = 2*a[0]/gamma
    for i in range(2,n+1):
        g[i] = 4*a[i-2]*a[i-1] / (b[i-2]*g[i-1])
    if n%2 == 0:
        g[-1] = coth(beta/4)**2
    return g

if __name__ == "__main__":
    for i in range(1,7):
        print(chebyshev(i,0.1))
