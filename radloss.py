#!/usr/bin/env python3
import numpy as np
import sys, argparse

parser = argparse.ArgumentParser()
parser.add_argument('--Ereff', type=float, help="effective relative permitivity", default=2)
parser.add_argument('--f', type=float, help="frequency in Hz", default=50e9)
parser.add_argument('--h', type=float, help="height of substrate in m", default=0.001)
args = parser.parse_args()

# https://www.circuitinsight.com/pdf/effect_radiation_losses_high_frequency_pcb_performance_ipc.pdf
h = args.h
lambda_0 = 2.998e8/args.f
ereff = args.Ereff
feff = 1.0 - ((ereff-1)/(2.0*np.sqrt(ereff))) * np.log((np.sqrt(ereff)+1)/(np.sqrt(ereff)-1))
ar = 60 * (2.0 * np.pi * h / lambda_0)**2*feff # Np/m
db_per_meter = ar * 8.686
print(db_per_meter, "dB per meter")
